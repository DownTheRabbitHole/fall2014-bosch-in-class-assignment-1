<?php
    //if all fields are set and not empty
    if (isset($_POST['ammo']) && !empty($_POST['ammo']) && isset($_POST['soldiers']) && !empty($_POST['soldiers'])
        && isset($_POST['duration']) && !empty($_POST['duration']) && isset($_POST['critique']) && !empty($_POST['critique'])){
        $ammo = (int)$_POST['ammo'];
        $soldiers = (int) $_POST['soldiers'];
        $duration = (float) $_POST['duration'];
        $critique = (string) $_POST['critique'];
        
        $mysqli = new mysqli('localhost', 'BoschAdmin', 'servicewithasmile', 'battlefield');
 
        if($mysqli->connect_errno) {
            printf("Connection Failed: %s\n", $mysqli->connect_error);
            exit;
        }
        $stmt = $mysqli->prepare("insert into reports (ammunition, soldiers, duration, critique) values (?,?,?,?)");
        if(!$stmt){
            printf("Query Prep Failed: %s<br>", $mysqli->error);
            exit;
        }
        $stmt->bind_param('iids', $ammo, $soldiers, $duration, $critique);
        $stmt->execute();
        $stmt->close();
        header("Location: battlefield-submit.html");
        
    }else{
        echo "Error: not all fields are set.";
        exit;
    }
    
    
?>