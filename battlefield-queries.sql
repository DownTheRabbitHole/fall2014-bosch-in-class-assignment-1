create database battlefield;

create table battlefield.reports(
    id mediumint unsigned not null auto_increment,
    primary key (id),
    ammunition smallint unsigned not null,
    soldiers smallint unsigned not null,
    duration double(6,1) unsigned not null,
    critique tinytext,
    posted timestamp not null default CURRENT_TIMESTAMP
    )engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;

