<!DOCTYPE html>
<head>
<title>Battlefield Analysis</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
 
<h1>Battlefield Analysis</h1><br>
<h2>Latest Critiques</h2><br>
<?php
    $mysqli = new mysqli('localhost', 'BoschAdmin', 'servicewithasmile', 'battlefield');
 
    if($mysqli->connect_errno) {
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
    }
       
    $stmt = $mysqli->prepare("select critique from reports order by posted limit 5");
    if(!$stmt){
        printf("Query Prep Failed: %s<br>", $mysqli->error);
        exit;
    }
    $stmt->execute();
    $stmt->bind_result($critique);
    echo "<ul>";
    while ($stmt->fetch()){
        printf("<li>%s</li>" ,
               htmlentities($critique)
               );
    }
    echo "</ul>";
    $stmt->close();
?>
<br>
<hr>
<h2>Battle Statistics</h2>
 <?php
    $stmt = $mysqli->prepare("select soldiers, avg(ammunition / duration) from reports group by soldiers order by soldiers desc");
    if(!$stmt){
        printf("Query Prep Failed: %s<br>", $mysqli->error);
        exit;
    }
    $stmt->execute();
    $stmt->bind_result($soldiers, $ammo_per_second);
    echo "<table><tr><th>Number of Soldiers</th><th>Pounds of Ammunition per Second</th></tr>";
    while ($stmt->fetch()){
        printf("<tr><td>%i</td><td>%f</td></tr>",
               htmlentities($soldiers),
               htmlentities($ammo_per_second)
              );
    }
    echo "</table>";
 ?>
<br><br>
<a href="battlefield-submit.html" >Submit a New Battle Report</a>
 
</div></body>
</html>